//
//  BusinessListApp.swift
//  BusinessList
//
//  Created by Gus Adi on 30/12/22.
//

import SwiftUI

@main
struct BusinessListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
