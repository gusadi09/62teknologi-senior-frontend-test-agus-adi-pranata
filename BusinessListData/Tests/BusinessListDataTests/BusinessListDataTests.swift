import XCTest
@testable import BusinessListData

final class BusinessListDataTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(BusinessListData().text, "Hello, World!")
    }
}
